# Lite YouTube Embed

This module provides an alternative field formatter for rendering YouTube
videos with [Paul Irish's Lite YouTube Embed library](https://github.com/paulirish/lite-youtube-embed)
instead of the default OEmbed iframe.

In order to allow generic usage on a remote video media bundle, that also
allows videos from other providers like Vimeo, the implementation falls back
to show the OEmbed iFrame for any other provider than YouTube.

[Issue Tracker](https://www.drupal.org/project/issues/lite_youtube_embed)

## Installation

It is recommended to use [Composer](https://getcomposer.org/) to get this
module with all dependencies:

```
composer require "drupal/lite_youtube_embed"
```

See the [Drupal documentation](https://www.drupal.org/docs/extending-drupal/installing-modules-composer-dependencies)
for more details.

### Paul Irish's Lite YouTube Embed library

The [Lite YouTube Embed](https://github.com/paulirish/lite-youtube-embed)
Javascript library is required for displaying the YouTube videos. There are
two ways to include the library:

**MANUAL INSTALLATION**

[Download the library](https://github.com/paulirish/lite-youtube-embed/releases)
and extract the file under the "libraries" directory. Ensure, that the library's
js and css files are found within the following path: ```libraries/lite-youtube-embed/src```

**INSTALLATION VIA COMPOSER**

If you have already installed the module via Composer, we recommend to do the
same with the Lite YouTube embed library. However, manual steps are required in
order to install Javascript libraries correctly (in the required destination
path) with Composer.

We highly recommend to use [Asset Packagist](https://asset-packagist.org) in
order to load Javascript libraries via Composer.

First, **copy the repositories snippet** from the module's composer.json file
into your project's composer.json file.

Next, the following snippet must be added into your project's composer.json
file so the javascript library is installed into the correct location:

```
"extra": {
    "installer-types": [
        "npm-asset"
    ],
    "installer-paths": {
        "web/libraries/{$name}": [
            "type:drupal-library",
            "type:npm-asset"
        ],
    }
}
```

If there are already 'repositories' and/or 'extra' entries in your project's
composer.json, merge these new entries with the already existing entries.

After that, run:

```
composer require "npm-asset/lite-youtube-embed"
```

## Configuration

1. Install the module
2. Use the "Lite YouTube embed (with oEmbed fallback)" field formatter for
   media or other entity types that should render YouTube videos instead of
   Core's oEmbed field formatter.
